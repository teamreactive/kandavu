const gulp = require('gulp'),
	browserSync = require('browser-sync').create(),
	sass = require('gulp-sass');

gulp.task('sass', () => {
	return gulp.src([
		// 'node_modules/bootstrap/scss/bootstrap.scss',
		'./scss/*.scss'
	])
	.pipe(sass.sync().on('error', sass.logError))
	.pipe(sass({errLogToConsole: true}))
	.pipe(sass({outputStyle: 'expanded'}))
	.pipe(gulp.dest('./'))
	.pipe(browserSync.stream());
});




gulp.task('js', () => {
	return gulp.src([
		'node_modules/bootstrap/dist/js/bootstrap.min.js',
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/popper.js/dist/umd/popper.min.js'
	])
	.pipe(gulp.dest('./js'))
	.pipe(browserSync.stream());
});

gulp.task('serve', ['sass'], () => {
	browserSync.init({
		// server: './src',
		proxy: "kandavu.local"
	});

	gulp.watch(['./scss/style.scss'], ['sass']);

	gulp.watch('./js/*.js').on('change', browserSync.reload);

	gulp.watch('./*.html').on('change', browserSync.reload);
	gulp.watch('./*.php').on('change', browserSync.reload);

});

gulp.task('font-awesome', () => {
	return gulp.src('node_modules/font-awesome/css/font-awesome.min.css')
	.pipe(gulp.dest('./css'));
});

gulp.task('fonts', () => {
	return gulp.src('node_modules/font-awesome/fonts/*')
	.pipe(gulp.dest('./fonts'));
});

gulp.task('default', ['js','serve','font-awesome','fonts']);